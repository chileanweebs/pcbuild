    (function($) {
    "use strict"; // Start of use strict
  
    // script para hacer que el scroll suave y no se vea drastico el cambio
    $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html, body').animate({
            scrollTop: (target.offset().top - 71)
          }, 1000, "easeInOutExpo");
          return false;
        }
      }
    });
  
    // Este js permite que cuando se llegue al final del contenido de la pantalla aparezca el boton de subir al tope.
    $(document).scroll(function() {
      var scrollDistance = $(this).scrollTop();
      if (scrollDistance > 100) {
        $('.scroll-to-top').fadeIn();
      } else {
        $('.scroll-to-top').fadeOut();
      }
    });
  
    // Este script cierra el menu responsivo cuando el link que dispara el scroll es clickeado
    $('.js-scroll-trigger').click(function() {
      $('.navbar-collapse').collapse('hide');
    });
  
    // Activa el seguimiento del scroll , para poder generar la expansion o colapso de la barra de navegacion y sus items segun el comportamiento del scroll
    $('body').scrollspy({
      target: '#mainNav',
      offset: 80
    });
  
    // Este script vuelve al tamaño sin expandir la barra de navegacion cuando nos alejamos mas del ancho normal que tiene dicha barra
    var navbarCollapse = function() {
      if ($("#mainNav").offset().top > 100) {
        $("#mainNav").addClass("navbar-shrink");
      } else {
        $("#mainNav").removeClass("navbar-shrink");
      }
    };
    // Mantiene la barra de navegacion minimizada cada vez que no estamos en ella
    navbarCollapse();
    // Minimiza la barra de navegacion cuando hacemos scroll
    $(window).scroll(navbarCollapse);
  
    // Este es párte de una plantilla del bootstrap , lo que hace es  darle etiquetas flotantes a las cabeceras de la forma de contacto
    $(function() {
      $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !!$(e.target).val());
      }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
      }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
      });
    });
  
  })(jQuery); // End of use strict
  