from django.db import models
from django.contrib.auth.models import User

class List(models.Model):
    correo = models.EmailField(max_length=200)
    username = models.CharField(max_length=200)
    password = models.CharField(max_length=200)

    def str(self):
        return self.correo


class Item(models.Model):
    text = models.CharField(max_length=500)
    List = models.ForeignKey(List, on_delete=models.CASCADE)
    complete = models.BooleanField()

    def str(self):
        return self.text
