from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegistrarUsario, LoginForm
from .models import List
from django.core.mail import send_mail
from django.contrib.auth.models import User
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)
from rest_framework import viewsets
from .serializers import UserSerializers

# Create your views here.

class UserViewset(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializers

def index(request):
    return render(request, 'app/index.html')

def cotizar(request):
    return render(request, 'app/cotizar.html')

def modelos(request):
    return render(request,'app/modelo.html')

def cuenta(request):
    return render(request, 'app/cuenta.html')

def confirmacion(request):
    return render(request, 'app/confirmacioniniciosesion.html')

def confirmacion2(request):
    return render(request, 'app/confirmacionregistro.html')

def registro(request):
    form1 = RegistrarUsario
    if request.method == "POST":
        form1 = RegistrarUsario(request.POST)

        if form1.is_valid():
            form1.save()
            messages.success(request,'Se ha registrado con exito!')
            return redirect("cuenta.html")
    else:
        form1 = RegistrarUsario()
    return render(request, "app/regusr.html", {"form1":form1})

def login1(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        data=form.cleaned_data
        user = authenticate(username=data.get("usr"), password=data.get("passwd"))
        if user is not None:
            login(request,user)
            messages.success(request,'Ha iniciado sesion correctamente!')
            return redirect("confirmacioniniciosesion.html")
        else:
            messages.warning(request,'Usuario y/o contraseña incorrectos!')
    return render(request,"app/login.html", {"form":form})