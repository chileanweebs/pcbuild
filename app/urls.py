from django.urls import  include, path
from django.contrib.auth import views as auth_views
from .views import index, cotizar, modelos, cuenta, registro, login1, confirmacion, confirmacion2, UserViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register('user', UserViewset)

urlpatterns = [
    path('index/index.html', index, name="index"),
    path('index/cotizar.html', cotizar, name="cotizar"),
    path('index/modelos.html', modelos, name="modelos"),
    path('index/cuenta.html', cuenta, name="cuenta"),
    path('index/confirmacioniniciosesion.html', confirmacion, name="confirmacion"),
    path('index/confirmacionregistro.html', confirmacion2, name="confirmacion2"),
    path('index/regusr.html', registro, name="regusr"),
    path('index/login.html', login1, name="login"),
    path('api/', include(router.urls)),
]