from django.test import SimpleTestCase
from django.urls import reverse, resolve
from app.views import index, cotizar, modelos, confirmacion, confirmacion2

class TestUrls(SimpleTestCase):

    def test_index(self):
        url = reverse('index')
        self.assertEquals(resolve(url).func, index)

    def test_cotizar(self):
        url = reverse('cotizar')
        self.assertEquals(resolve(url).func, cotizar)
    
    def test_modelos(self):
        url = reverse('modelos')
        self.assertEquals(resolve(url).func, modelos)

    def test_confirmacion(self):
        url = reverse('confirmacion')
        self.assertEquals(resolve(url).func, confirmacion)
    
    def test_confirmacion2(self):
        url = reverse('confirmacion2')
        self.assertEquals(resolve(url).func, confirmacion2)